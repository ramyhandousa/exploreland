<?php

namespace App\Http\Controllers\Admin;

use App\Ad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageName = 'الإعلانات التروجية';
        $ads = Ad::all();
        return view('admin.ads.index',compact('pageName','ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageName = '   إعلان ترويجي';
        return view('admin.ads.create',compact('pageName'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $ads = new Ad();
        $ads->title = $request->title;
        $ads->link  = $request->link;

            if ($request->hasFile('image')):
                $name=  time() . '.' . str_random(20) .$request->image->getClientOriginalName();
                $request->image->move(public_path().'/files/products/', $name);
                $ads->url =  '/public/files/products/' .$name;
            endif;
         $ads->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'الإعلان']),
            "url" => route('ads.index'),

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ads = Ad::find($id);
        $pageName = '   تعديل الإعلان';

        return view('admin.ads.edit',compact('ads' ,  'pageName'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ads =  Ad::find($id);
        $ads->title = $request->title;
        $ads->link  = $request->link;

        if ($request->hasFile('image')):
            $name=  time() . '.' . str_random(20) .$request->image->getClientOriginalName();
            $request->image->move(public_path().'/files/products/', $name);
            $ads->url =  '/public/files/products/' .$name;
        endif;
        $ads->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'الإعلان']),
            "url" => route('ads.index'),

        ]);
    }

    public function delete(Request $request)
    {
        $model = Ad::findOrFail($request->id);

        if ($model->delete()) {

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }
}
