<?php

namespace App\Http\Controllers\Admin;

use App\Models\NewAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use UploadImage;

class NewsAdminController extends Controller
{
    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/categories/';
    }

    public function index(Request $request)
    {

        $categories = NewAdmin::latest()->get();


        $pageName = 'إدارة  الأخبار';

        return view('admin.news.index',compact('categories','pageName'));
    }
    public function create()
    {
        $cats = NewAdmin::where('is_suspend',0)->get();

        $pageName = '   اسم  الخبر';

        return view('admin.news.create')->with(compact('cats','pageName'));
    }

    public function store(Request $request)
    {

        $NewAdmin = new NewAdmin;

        $NewAdmin->{'name:ar'} = $request->name_ar;
        $NewAdmin->{'description:ar'} = $request->description_ar;

        if ($request->hasFile('image')):
            $NewAdmin->image = '/public/' .  $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;

        if ($NewAdmin->save()) {

            $url =  route('newsAdmin.index');
            $name = ' الخبر  ';

            return response()->json([
                'status' => true,
                "message" => __('trans.addingSuccess',['itemName' => $name]),
                "url" => $url,

            ]);

        }

    }

    public function edit($id)
    {

        $NewAdmin = NewAdmin::findOrFail($id);
        $pageName = '         الخبر';

        return view('admin.news.edit')->with(compact('NewAdmin', 'cats', 'pageName' , 'subSubCategories'));

    }


    public function update(Request $request, $id)
    {

        $NewAdmin = NewAdmin::findOrFail($id);
        $NewAdmin->{'name:ar'} = $request->name_ar;
        $NewAdmin->{'description:ar'} = $request->description_ar;
        if ($request->hasFile('image')):
            $NewAdmin->image =  '/public/' .  $this->public_path . UploadImage::uploadImage($request, 'image', $this->public_path);
        endif;
        if ($NewAdmin->save()) {
            $url =  route('newsAdmin.index');
            $name = '   الخبر';
            return response()->json([
                'status' => true,
                "message" => __('trans.editSuccess',['itemName' => $name]),
                "url" => $url,
            ]);
        }

    }



    public function delete(Request $request){

        $model = NewAdmin::findOrFail($request->id);

        if ($model->delete()) {

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }

    }

    public function suspend(Request $request)
    {
        $model = NewAdmin::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر الخبر بنجاح";
        } else {
            $message = "لقد تم فك الحظر على الخبر بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }

}
