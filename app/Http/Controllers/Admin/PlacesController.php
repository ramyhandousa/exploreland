<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\City;
use App\Models\Trip;
use App\TripImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageName = 'الأمكان';
        $products = Trip::all();

        return view('admin.products.index',compact('products','pageName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageName = 'الأمكان';
        $categories = Category::whereParentId(null)->whereIsSuspend(0)->get();
//        $cities = City::where('parent_id',0)->whereHas('children')->get();
        $cities = City::where('parent_id',0)->get();

        return view('admin.products.create',compact('products','categories','pageName','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $trip = new Trip();
        $trip->category_id          = $request->categoryId;
        $trip->price                = $request->price;
        $trip->date                 = $request->date_trip;
        $trip->phone                = $request->phone;
        $trip->email                = $request->email;
        $trip->city_id              = $request->cityId;
        $trip->{'name:ar'}          = $request->name_ar;
        $trip->{'description:ar'}   = $request->description_ar;
        $trip->can_rate             = $request->is_rate ? 1: 0;
        $trip->can_comment          = $request->is_comment ? 1 : 0;
        $trip->save();

        if ($request->hasFile('image')):
            foreach($request->file('image') as $image)
            {
                $name=  time() . '.' . str_random(20) .$image->getClientOriginalName();
                $image->move(public_path().'/files/products/', $name);
                $images = new  TripImage();
                $images->trip_id = $trip->id;
                $images->url =  '/public/files/products/' .$name;
                $images->save();
            }
        endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'المكان']),
            "url" => route('places.index'),

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trip = Trip::findOrFail($id);
        $pageName = '  تعديل '.$trip->name .'  الرحلة ';
        $categories = Category::whereParentId(null)->whereIsSuspend(0)->get();
//        $cities = City::where('parent_id',0)->whereHas('children')->get();
        $cities = City::where('parent_id',0)->get();

        return view('admin.products.edit',compact('trip', 'categories', 'pageName' , 'cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $trip =   Trip::find($id);
        $trip->category_id          = $request->categoryId;
        $trip->price                = $request->price;
        $trip->date                 = $request->date_trip;
        $trip->phone                = $request->phone;
        $trip->email                = $request->email;
        $trip->city_id              = $request->cityId;
        $trip->{'name:ar'}          = $request->name_ar;
        $trip->{'description:ar'}   = $request->description_ar;
        $trip->can_rate             = $request->is_rate ? 1: 0;
        $trip->can_comment          = $request->is_comment ? 1 : 0;
        $trip->save();

        if ($request->hasFile('image')):
            foreach($request->file('image') as $image)
            {
                $name=  time() . '.' . str_random(20) .$image->getClientOriginalName();
                $image->move(public_path().'/files/products/', $name);
                $images = new  TripImage();
                $images->trip_id = $trip->id;
                $images->url =  '/public/files/products/' .$name;
                $images->save();
            }
        endif;

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'المكان']),
            "url" => route('places.index'),

        ]);
    }

    public function suspend(Request $request)
    {
        $model = Trip::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر بنجاح";
        } else {
            $message = "لقد تم فك الحظر بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }


    public function delete(Request $request)
    {
        $model = Trip::findOrFail($request->id);

        if ($model->delete()) {

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }


    public function deleteImage(Request $request)
    {
        $previousworks = TripImage::findOrFail($request->id);

        if ($previousworks->delete()) {
            return response()->json([
                'status' => true,
                'data' => $previousworks->id
            ]);
        }
    }
}
