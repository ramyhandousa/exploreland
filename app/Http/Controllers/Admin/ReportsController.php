<?php

namespace App\Http\Controllers\Admin;

use App\Models\BankTransfer;
use App\Models\OrderProduct;
use App\Models\OrderProject;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Gate;
class ReportsController extends Controller
{

    public function index(Request $request)
    {
        if ($request->type == 'projects'){

            $pageName = 'تقارير طلبات المشاريع ';
            $reports = User::where(function ($q) use ($request){
                                    $q->where('name','like', '%' . $request->name . '%')
                                    ->orWhere('phone','like', '%' . $request->name . '%');
                            })->whereHas('provider_projects')
                            ->latest()->get();

        }else{
            $pageName = 'تقارير طلبات المنتجات ';

            $reports = User::where(function ($q) use ($request){
                    $q->where('name','like', '%' . $request->name . '%')
                        ->orWhere('phone','like', '%' . $request->name . '%');
                })
                ->whereHas('provider_products')
                ->latest()->get();
        }

        return view('admin.Reports.index', compact('reports','pageName'));
    }


    public function gym_data(Request $request)
    {
        $id = $request->id;

        $filter =function ($q){
            $q->select('id','name');
        };
        if ($request->type == 'projects'){

            $pageName = 'تفاصيل طلبات المشاريع ';

            $reports = OrderProject::where('provider_id',$id)->with(['user' => $filter])->latest()->get();

        }else{

            $pageName = 'تفاصيل طلبات المنتجات ';

            $reports = OrderProduct::where('provider_id',$id)->with(['user' => $filter])->latest()->get();

        }

        $reports->map(function ($q){
            $q->adminPrice =  ($q->price * $q->app_percentage ) /100;
            $q->providerPrice = $q->price -  $q->adminPrice;
        });

        $provider = $reports->pluck('provider')->first();

        $payForProvider = $reports->sum('providerPrice');

        return view('admin.Reports.show', compact('reports','pageName','provider' , 'payForProvider'));
    }



}
