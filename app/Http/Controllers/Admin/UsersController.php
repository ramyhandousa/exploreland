<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\GymValid;
use App\Mail\AcceptedAccount;
use App\Mail\refuseAccount;
use App\Notifications\sendEmailToNewAccount;
use App\User;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Mail;
use Silber\Bouncer\Database\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use UploadImage;
use Validator;

class UsersController extends Controller
{


    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/users/';
    }
    
    public function index(Request $request)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $quary = User::whereDoesntHave('roles')->whereDoesntHave('abilities');

        if ($request->type == 'provider'){
            $quary->where('is_active', 1)->where('defined_user','provider');
            $pageName = 'إدارة مزودي الخدمات';
        }else{
            $quary->where('defined_user','user');
            $pageName = 'إدارة المستخدمين';
        }

        $users =  $quary->latest()->get();

        return view('admin.users.index', compact('users' , 'pageName'));

    }


  
    public function create()
    {

        $cities = City::where('is_suspend',0)->whereParentId(null)->get();
        return view('admin.users.add', compact('cities'));
    }

    public function testImageView(Request $request){



        $cities = City::where('is_suspend',0)->whereParentId(null)->get();
        return view('admin.users.testing', compact('cities'));
    }


    public function testImage(Request $request){

        return $request->all();
    }

  
    public function store(GymValid $request)
    {

        $data =  $request->all();


        //  $filtered To make Sure Provider Choose One Day
        $filtered = collect($data['day'])->filter(function ($value, $key) {
            return $value['start'] != null && $value['end'] != null;
        });
            if ($filtered->count() == 0){

                return \Redirect::back()->withErrors(['يجب اختيار يوم واحد علي الاقل بداية و نهاية له في الاسبوع', 'The Message']);
            }

        $user               = new User;
        $user->defined_user = 'gym';
        $user->city_id      = $request->city;
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->phone        = $request->phone;
        $user->password     = $request->password;
        $user->address      = $request->address;
        $user->latitute     = $request->latitute;
        $user->longitute     = $request->longitute;
        $user->price        = $request->price;
        $user->description  = $request->description;
        $user->api_token = str_random(60);
        $user->is_active    = 1;
        $user->save();

        Mail::to($user->email)->send(new AcceptedAccount($user, $request));

        $this->createGalleryGym($request, $data , $user);

        if ($filtered->count() > 0){
            $this->createDayForGym($data,$user);
        }

        session()->flash('success', 'لقد تم إضافة الجيم بنجاح.');
        return redirect()->route('users.index','type=gym');
    }

    public function show($id)
    {

        $user = User::findOrFail($id);


        return view('admin.users.show', compact('user' ));
    }
    


    public function edit($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }

        return view('admin.users.edit');
    }

   
    public function update(Request $request, $id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        
    }
    
    public function destroy($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
       
    }
    
    
    public function accpetedUser(Request $request){
        $user = User::findOrFail($request->id);
        
        if ($user){
            $user->update(['is_accepted' => 1]);

            try {
                Mail::to($user->email)->send(new AcceptedAccount($user, $request));

            } catch (\Exception $e) {

            }


            return response()->json( [
                'status' => true ,
            ] , 200 );
            
        }else{
            
            return response()->json( [
                'status' => false
            ] , 200 );
            
        }
        
    }



    public function refuseUser(Request $request){
        $user = User::findOrFail($request->id);

        $user->update(['is_accepted' => -1,'message' => $request->message]);

        try {
                Mail::to($user->email)->send(new refuseAccount($user));

        } catch (\Exception $e) {

        }

        session()->flash('success', 'لقد تم  رفض مزود الخدمة  بنجاح.');
        return redirect()->route('users.index','type=provider');
    }




    public function reAcceptUser(Request $request){
        $user = User::findOrFail($request->id);

        $user->update(['is_accepted' => 1,'message' => null]);

        try {

            Mail::to($user->email)->send(new AcceptedAccount($user, $request));

        } catch (\Exception $e) {
            // report($e);

            // return false;
        }

        return response()->json( [
            'status' => true ,
        ] , 200 );
    }


    public function suspendUser(Request $request)
    {
        $model = User::findOrFail($request->id);


        $model->is_suspend = $request->type;
        $model->message = $request->message;
        if ($request->type == -1) {

            $message = "لقد تم حظر  بنجاح";

        } else {
            $model->message = null;
            $message = "لقد تم فك الحظر بنجاح";
        }

        if ($model->save()) {
            session()->flash('success',$message);
            return redirect()->back();
        }

    }





}
