<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\categoryRecource;
use App\Http\Resources\listMealRecource;
use App\Http\Resources\ProductFilter;
use App\Http\Resources\tripResourceFilter;
use App\Models\Category;
use App\Models\City;
use App\Models\Product;
use App\Models\Trip;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{

    public $headerApiToken;


    public function __construct( )
    {
//        $this->middleware('apiToken', [ 'except' =>  [ 'show']  ]);

        $language = request()->headers->get('Accept-Language') ? : 'ar';
        App::setLocale($language);

        $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';



    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {


    }



    public function create()
    {
        //
    }


    public function store(Request $request)
    {

    }


    public function show($id)
    {
        return 'safsa';
//        $category = Category::find($id);

//        if (!$category){  return $this->CategoryNotFound();  }
//
//        $query = Trip::latest();
//
//        $this->paginationNews($request, $query);
//
//        if ($request->categoryId){
//            $query->where('category_id' , $request->categoryId);
//        }
//
//        if ($request->cityId){
//            $city = City::whereId($request->cityId)->with('children')->first();
//            if($city){
//                $childrenIds =  $city['children']->pluck('id');
//
//                $query->where('city_id',$request->cityId)->orWhereIn('city_id',$childrenIds)->get();
//
//
//            }
//        }
//
//        $trips = $query->get();
//
//
//        $data  =  tripResourceFilter::collection($trips);
//
//        return response()->json( [
//            'status' => 200 ,
//            'data' => $data ,
//        ] , 200 );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }



    function paginationNews($request , $query){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1

        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
    }


    private  function CategoryNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'هذا القسم غير موجود '   ],200);
    }


    private  function CategoryFoundBefore(){
        return response()->json([   'status' => 400,  'error' => (array)'هذا القسم لديك مسبقا '   ],200);
    }
}
