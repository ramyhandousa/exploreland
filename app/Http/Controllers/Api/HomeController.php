<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CityFilter;
use App\Http\Resources\CityRecource;
use App\Http\Resources\homeResource;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        $data = new  homeResource($request);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    public function country(Request $request , Country $countery){

        $cities =  $countery->newQuery();

        if ($request->has('subCity')) {

            $filter  =   $countery->whereParentId($request->subCity);

        }else{

            $filter = $cities->whereHas('children')->whereParentId(0);
        }

        $filterSelected = $filter->where('is_suspend',0)->select('id','name_ar','flag')->get();

         $data = CityFilter::collection($filterSelected);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


}
