<?php

namespace App\Http\Controllers\api;
use App\Http\Resources\UserRecource;
use App\Models\Device;
use App\User;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Validator;
class LoginController extends Controller
{
       
       public $headerApiToken;
       public function __construct( )
       {
	    $language = request()->headers->get('Accept-Language') ?  : 'ar';
	   app()->setLocale($language);
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
       
       }
       
    public function login(Request $request){

      if ($user =  Auth::attempt([  'email' => $request->email, 'password' => $request->password  ])  ) {

          $user =  Auth::user();

           $reason = auth()->user()->message;
          
           if ($user->is_suspend == -1) {
                return response()->json( [
                    'status' => 400 ,
                    'error' =>  (array) __('global.suspendBecause', ['phone' => Setting::getBody('phone_contact'), "reason" => $reason])
                ] , 200 );
            }

          $data = new UserRecource($user);

          return response()->json([
              'status' => 200,
              'message' =>     trans('global.logged_in_successfully'),
              'data' => $data
          ]);

      }else{

          return response()->json([
              'status' => 401,
              'error' => (array)   trans('global.username_password_notcorrect') ,
          ],200);

      }
  }
    
    public function logOut(Request $request){
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();
        $user->update(['api_token' => str_random(60)]);
        return response()->json( [
            'status' => 200 ,
            'message' => trans('global.logged_out_successfully') ,
        ] , 200 );
    }
   
    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

}
