<?php

namespace App\Http\Controllers\api;

use App\Http\Helpers\Sms;
use App\Http\Requests\api\resgister;
use App\Mail\mailActiveAccount;
use App\Models\City;
use App\Models\Day;
use App\Models\PlaceWorkingTime;
use App\Models\Countery;
use App\Models\Device;
use App\Models\Profile;
use App\Models\VerifyUser;
use App\User;
use App\Models\UserSetting ;
use App\Models\PlaceGeneralInfo ;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;


class RegisterController extends Controller
{
       public $public_path;
       public $defaultImage; 
       
       public function __construct( )
       {
	    $language = request()->headers->get('Accept-Language') ? request()->headers->get('Accept-Language') : 'ar';
	    app()->setLocale($language);
     
	    $this->public_path = 'files/users/profiles/';
	    
	    // default Image upload in profile user
	    $this->defaultImage = \request()->root() . '/' . 'public/assets/admin/images/defualt_User.png';
	     
	    
       }
       
    public function register(resgister $request){

        $city  = City::where('is_suspend', 0)->whereId($request->cityId)->whereHas('parent')->first();

        if (!$city){  return $this->CityNotFound();  }

	    $action_code = substr( rand(), 0, 4);

        $user                   = new User();
        $user->defined_user     =   'user';
        $user->name             =   $request->name;
        $user->phone            =   $request->phone;
        $user->email            =   $request->email;
        $user->password         =   $request->password;
        $user->city_id          =    $city->id;
        $user->is_active        =   0;
        $user->api_token        = str_random(60);
        $user->save();

        $this->createVerfiy($request , $user , $action_code);

         try {
           // Mail::to($user->email)->send(new mailActiveAccount($user,$action_code));

         } catch (\Exception $e) {

        }
//	    Sms::sendMessage('Activation code:' . $action_code, $request->phone);
        $data = ['code' => $action_code];
        return response()->json( [
            'status' => 200 ,
            'data' => $data,
            'message' =>trans('global.activation_code_sent')
        ] , 200 );
    }

   private function createVerfiy($request , $user , $action_code){
       $verifyPhone = new VerifyUser();
       $verifyPhone->user_id =  $user->id;
       $verifyPhone->phone =    $request->phone;
       $verifyPhone->action_code = $action_code;
       $verifyPhone->save();
   }


    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }
  
}
