<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\listPlaces;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;

class SearchController extends Controller
{
    public function filterData(Request $request){


        $query = User::whereDoesntHave('roles')->whereDoesntHave('abilities')
                        ->whereIsActive(1)
                        ->where('is_accepted',1)
                        ->whereIsSuspend(0)
                        ->where('defined_user','!=','other');

        if ($request->name){
            $this->filterName($request,$query);
        }
        if ($request->cityId){
            $this->filterCity($request,$query);
        }
        if ($request->type){
            $this->filterType($request,$query);
        }
        if ($request->classifications){
            $this->filterClassifications($request,$query);
        }

        if ($request->keywords){

            $places = $this->filterKeywords($request,$query);

        }else{

            $places = $query->with('ratings')->get();
        }

        $data = listPlaces::collection($places);
        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    private function filterName($request ,$query){
        $query->whereRaw('CONCAT(first_name, " ", last_name) LIKE ? ', '%' . $request->input('name') . '%');
    }

    private function filterCity($request ,$query){
        $query->where('city_id',$request->cityId);
    }

    private function filterType($request ,$query){
        $query->where('defined_user',$request->type);
    }

    private function filterClassifications($request ,$query){
        $query->whereHas('placeInformation',function ($q) use ($request){
            $q->where('classifications', $request->classifications);
        });
    }


    private function filterKeywords($request ,$query){

        $key =   array_map( 'trim', explode( ",", $request->keywords ) );
        $data = $query->Where(function ($query) use($key) {
            for ($i = 0; $i < count($key); $i++){
                $query->orwhere('keywords', 'like',  '%' . $key[$i] .'%');
            }

        })->get();

        return $data;

    }

}
