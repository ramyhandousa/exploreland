<?php

namespace App\Http\Controllers\Api;

use App\Libraries\InsertNotification;
use App\Models\Bank;
use App\Models\City;
use App\Models\CityTranslation;
use App\Models\Device;
use App\Models\OrderOffer;
use App\Models\OrderProduct;
use App\Models\OrderProject;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypesSupport;
use App\Models\TypesSupportTranslation;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use UploadImage;
use App\Libraries\PushNotification;

class SettingController extends Controller
{
       public $public_path;
       public $headerApiToken;
       public $language;
       public  $notify;
        public $push;
       public function __construct(InsertNotification $notification,PushNotification $push)
       {
           $language = request()->headers->get('Accept-Language') ? : 'ar';
           app()->setLocale($language);
           $this->language = $language;

	    $this->public_path = 'files/transfers';

           $this->middleware('apiToken')->only(['contactUs']);

           // api token from header
	    $this->headerApiToken = request()->headers->get('apiToken') ? request()->headers->get('apiToken') : ' ';
	    $this->notify = $notification;
           $this->push = $push;

       }

       public function aboutUs(){

       $about_us =  Setting::where('key','about_us_ar')->first();

       $data = ['about_us' => $about_us ? $about_us->body : ''  ];

        return response()->json( [
            'status' => 200 ,
            'data' => $data  ,
        ] , 200 );

       }
   
       public function terms_user(){
           $setting = Setting::where('key','terms_user_ar' )->first();

           $data = ['terms_user' => $setting ? $setting->body : ''  ];
            return response()->json( [
                    'status' => 200 ,
                    'data' => $data ,
            ] , 200 );
       }
   
    public function contactUs(Request $request){
	    // check for user
        $user = User::where( 'api_token' , $this->headerApiToken )->first();
        $support = new Support();
        $support->user_id = 1;
        $support->sender_id = $user->id;
        $support->message = $request->message;
        $support->save();
         return response()->json( [
        'status' => 200 ,
        'message' => trans('global.message_was_sent_successfully'),
        ] , 200 );
   }
       

       
   private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
   }

   private  function typeSupportNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'برجاء اختيار قسم التواصل'   ],200);
   }

       
}
