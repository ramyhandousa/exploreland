<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\api\commentvalid;
use App\Http\Requests\api\rate;
use App\Http\Resources\tripResource;
use App\Http\Resources\tripResourceFilter;
use App\Models\Category;
use App\Models\City;
use App\Models\Trip;
use App\Models\TripComment;
use App\Models\TripRate;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trip = Trip::whereId($id)->first();
        if (!$trip){
            return $this->TripNotFound();
        }
        $data  =  new tripResource($trip);
        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function trip_category(Request $request){

        $query = Trip::latest();

        $this->paginationNews($request, $query);

        if ($request->categoryId){

            $category = Category::find($request->categoryId);

            if (!$category){  return $this->CategoryNotFound();  }

            $query->where('category_id' , $request->categoryId);
        }

        if ($request->cityId){
            $city = City::whereId($request->cityId)->with('children')->first();
            if($city){
                $childrenIds =  $city['children']->pluck('id');

                $query->where('city_id',$request->cityId)->orWhereIn('city_id',$childrenIds)->get();

            }
        }

        $trips = $query->get();

        $data  =  tripResourceFilter::collection($trips);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }


    function paginationNews($request , $query){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1

        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
    }


    public function rate_trip(rate $request){
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        $trip = Trip::whereId($request->tripId)->first();
        if (!$trip){
            return $this->TripNotFound();
        }

        if ($trip->can_rate == 0){
            return $this->TripCanRate();
        }

        $rating = new TripRate();

        $userRatingBefore = $rating->where('trip_id', $trip->id)->where('user_id', $user->id)->first();

        if ($userRatingBefore) {

            $userRatingBefore->rate = $request->rateValue;
            $userRatingBefore->save();

        }else{

            $rating->user_id = $user->id;
            $rating->trip_id = $trip->id;
            $rating->rate    = $request->rateValue;
            $rating->save();
        }
        $data  =  new tripResource($trip);
        return response()->json( [
            'status' => 200 ,
            'message' =>    trans('global.rate_success'),
            'data' => $data

        ] , 200 );
    }

    public function comment_trip(commentvalid $request){

        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        $trip = Trip::whereId($request->tripId)->first();
        if (!$trip){
            return $this->TripNotFound();
        }

        if ($trip->can_comment == 0){
            return $this->TripCanComment();
        }

        $rating = new TripComment();

        $userRatingBefore = $rating->where('trip_id', $trip->id)->where('user_id', $user->id)->first();

        if ($userRatingBefore) {

            $userRatingBefore->comment = $request->comment;
            $userRatingBefore->save();

        }else{

            $rating->user_id    = $user->id;
            $rating->trip_id    = $trip->id;
            $rating->comment    = $request->comment;
            $rating->save();
        }

        $data  =  new tripResource($trip);

        return response()->json( [
            'status' => 200 ,
            'message' =>    'تم التعليق بنجاح',
            'data' => $data
        ] , 200 );
    }



    private  function TripNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'هذه الرحلة  غير موجودة '   ],200);
    }
    private  function CategoryNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'هذا القسم غير موجود '   ],200);
    }


    private  function TripCanComment(){
        return response()->json([   'status' => 400,  'error' => (array)'لا يسمح لك بالتعليق علي هذه الرحلة '   ],200);
    }

    private  function TripCanRate(){
        return response()->json([   'status' => 400,  'error' => (array)'لا يسمح لك بالتقيم علي هذه الرحلة '   ],200);
    }


}
