<?php

namespace App\Http\Controllers\Api;

use App\Ad;
use App\Http\Requests\api\editUser;
use App\Http\Requests\testingPassword;
use App\Http\Resources\homeResource;
use App\Http\Resources\newResource;
use App\Http\Resources\UserFilterRecource;
use App\Http\Resources\UserRecource;
use App\Models\Category;
use App\Models\City;
use App\Models\NewAdmin;
use App\Models\VerifyUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\api\rate;

use Carbon\Carbon;

class UserController extends Controller
{

    public $public_path;
    public function __construct( )
    {
      //  $this->middleware('apiToken', [ 'except' =>  ['index','show','show_my_work']  ]);

        $language = request()->headers->get('Accept-Language') ? request()->headers->get('Accept-Language') : 'ar';
        app()->setLocale($language);

        $this->public_path = 'files/users/profiles';



    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = new  homeResource($request);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    function paginationNews($request , $query){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1

        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
    }
    public function show($id ,Request $request)
    {
        $query = User::whereId($id)
            ->whereDoesntHave('roles')
            ->whereDoesntHave('abilities')
            ->whereIsActive(1)
            ->where('is_accepted', 1)
            ->whereIsSuspend(0);

        if ($request->categories){
             $query->with('categories');
        }

        if ($request->my_works){
            $query->with('my_works');
        }

        $user = $query->first();

        if (!$user){  return $this->UserNotFound();  }

        $data = new UserFilterRecource($user);

        return response()->json( [
            'status' => 200 ,
            'data' => $data ,
        ] , 200 );
    }

    public function editProfile(editUser $request){



        $data = $request->all();

        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        if ($request->cityId){
            $city  = City::where('is_suspend', 0)->whereId($request->cityId)->whereHas('parent')->first();

            if (!$city){  return $this->CityNotFound();  }
            $user->city_id = $data['cityId'];
        }


         $action_code = substr(rand(), 0, 4);

        $checkPhoneChange = $this->changeMyPhone($request , $user , $action_code);

        $filterData =  collect($data)->except('phone');

        $user->fill($filterData->toArray());

        if ($request->image){
            $user->image =   '/public/' . $this->public_path .\UploadImage::uploadImage( $request , 'image' , $this->public_path );
        }

        if ($request->newPassword){
            $user->password =   $request->newPassword;
        }

        $user->save();

        $data = new UserRecource($user);

        return response()->json( [
            'status' => 200 ,
            'code' => $request->phone&&$checkPhoneChange == 0 ?  (string)$action_code : '',
            'message' => trans('global.profile_edit_success') ,
            'data' => $data ,
        ] , 200 );
    }

    function changeMyPhone($request , $user ,$action_code ){

        if ($request->phone){

            $checkPhoneChange = $user->wherePhone($request->phone)->count();

            if ($checkPhoneChange == 0){

                $foundPhone = VerifyUser::whereUserId($user->id)->first();

                if ($foundPhone){

                    $foundPhone->update(['phone' =>$request->phone , 'action_code' => $action_code ]);

                }else{

                    $verify = new  VerifyUser();
                    $verify->user_id= $user->id;
                    $verify->phone = $request->phone;
                    $verify->action_code = $action_code;
                    $verify->save();
                }
            }

            return $checkPhoneChange;
        }

    }
    
    
    public function ratePlace(rate $request){

        $user = Auth::user();

        $place = User::whereId($request->placeId)->first();

        if ( ! $place ) {   return $this->placeNotFound();  }

        $rating = new Rating();

        $userRatingBefore = $rating->where('rateable_id', $place->id)->where('user_id', $user->id)->first();

        if ($userRatingBefore) {

            $rating = Rating::whereId($userRatingBefore->id)->first();
            $rating->user_id = $user->id;
            $rating->rating = $request->rateValue;
            $rating->comment = $request->comment;
            $place->ratings()->save($rating);

        }else{
            $rating->user_id = $user->id;
            $rating->rating = $request->rateValue;
            $rating->comment = $request->comment;
            $place->ratings()->save($rating);
        }

        return response()->json( [
            'status' => 200 ,
            'message' =>    trans('global.rate_success'),
        ] , 200 );
    }



    public function testChangePassword(testingPassword $request){

         $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        return "ok";


    }

    private  function UserNotFound(){
        return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
    }

    private  function providerNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'مزود الخدمة غير موجود '   ],200);
    }


    private  function CityNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'المدينة او الدولة غير موجودة'   ],200);
    }

    private  function workNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)'لا يوجد سابقة اعمال '   ],200);
    }

    private  function KeywordNotFound(){
        return response()->json([   'status' => 400,  'error' => (array)' الكلمات الدلية غير موجودة'   ],200);
    }
    
     private  function placeNotFound(){
        return response()->json([   'status' => 400,  'error' => (array) 'هذا المكان غير موجود لدينا'   ],200);
    }

}
