<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditGym extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        return [
            'phone' => 'required|unique:users,phone,'. $user->id ,
            'email' => 'required|unique:users,email,'. $user->id ,
//            'image' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'phone.required' => 'رقم الهاتف مطلوب',
            'phone.unique' => 'هذا الرقم مستخدم من قبل',
            'email.required' => 'البريد الإلكتروني مطلوب',
//            'image.required' => 'إختيار صورة واحدة علي الاقل لتكون بروفيل للجيم',
            'email.unique' => 'هذا البريد مستخدم من قبل',
        ];
    }

    public function withValidator($validator)
    {

        $validator->after(function ($validator) {

            if (  count($this->image) > 1 ){

                $validator->errors()->add('files', 'يجب اختيار صورة واحدة فقط ');
            }

        });
        return;
    }
}
