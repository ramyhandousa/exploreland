<?php

namespace App\Http\Requests\api;

use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class editUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::whereApiToken(request()->headers->get('apiToken'))->first();

        return [
            'phone' => 'unique:users,phone,' . $user->id ,
            'email' => 'email|unique:users,email,' . $user->id ,
        ];
    }


    public function messages()
    {
        return [
            'email.unique' => trans('global.unique_email'),
            'phone.unique' => trans('global.unique_phone'),
        ];
    }

    public function withValidator($validator)
    {
        // checks user current password
        // before making changes
        $validator->after(function ($validator) {

            $user = User::where('api_token', request()->headers->get('apiToken'))->first();

            if ( $this->oldPassword ) {

                if (! Hash::check( $this->oldPassword , $user->password ) ) {

                    $validator->errors()->add('newPhone',  trans('global.old_password_is_incorrect')   );
                }
            }

        });
        return;
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
