<?php

namespace App\Http\Requests;

use App\Rules\change_password;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class testingPassword extends FormRequest
{


    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        return [
           'oldPassword' => [   new change_password($user ,  \request()) ]
        ];
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
