<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CityFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data  = [
            'id'       =>  $this->id,
            'name'     => $this->name_ar,
            'flag'     => $this->when($this->flag ,  \URL::to('/') .'/'. $this->flag),
        ];

        return $data;
    }
}
