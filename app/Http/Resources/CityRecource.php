<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CityRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data  = [
            'id'       =>  $this->id,
            'name'     => $this->name_ar,
            'flag'     => $this->when($this->flag ,  \URL::to('/') . $this->flag),
            'country'   => $this->when($this->parent   , new CityRecource($this->parent) )
        ];

        return $data;
    }
}
