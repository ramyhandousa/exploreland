<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserFilterRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'    => $this->id,
            'name'   => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'image' =>  $this->when($this->image ,  \URL::to('/') . $this->image) ,
            'categories' => categoryRecource::collection($this->categories),
            'my_works' => PreviousWorkFilter::collection($this->whenLoaded('my_works')),
        ];
    }
}
