<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;

class UserRecource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'image' => $this->when($this->image ,  \URL::to('/') . $this->image) ,
            'city' =>  $this->when($this->city_id , new CityRecource($this->city)),
            'is_active' => $this->is_active ? 1  == true : false,
            'is_suspend' => $this->is_suspend ? 1  == true : false,
            'message' =>  $this->when($this->message ,  $this->message),
            $this->mergeWhen(
                    ($request->route()->uri ==  'api/Auth/login') ||
                    ($request->route()->uri ==   'api/Auth/checkCode')||
                    ($request->route()->uri == 'api/Auth/editProfile'),
                [
                    'api_token' => $this->when($this->api_token , $this->api_token) ,

                ]
            ),
        ];
    }


}
