<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class adsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'title' => $this->title,
            'link'  => $this->link,
            'url' => $this->when($this->url ,  \URL::to('/') . $this->url) ,
        ];
    }
}
