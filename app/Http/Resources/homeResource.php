<?php

namespace App\Http\Resources;

use App\Ad;
use App\Models\Category;
use App\Models\NewAdmin;
use Illuminate\Http\Resources\Json\JsonResource;

class homeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $query = NewAdmin::latest();

        $this->paginationNews($request, $query);

        $ads = Ad::latest()->get();
        $categories = Category::whereParentId(null)->whereIsSuspend(0)->get();

        $data  = [
            'ads'           => $this->when($request->ads , adsResource::collection($ads)),
            'categories'   => $this->when($request->categories ,   categoryRecource::collection($categories)),
            'news'         => newResource::collection( $query->get() )
        ];

        return $data;

    }


    function paginationNews($request , $query){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1

        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
    }
}
