<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class newResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data  = [
            'id'            =>  $this->id,
            'name'         => $this->name,
            'image'         => \URL::to('/') . $this->image,
            'created_at'     => $this->created_at->format('Y-m-d')
        ];

        return $data;
    }



}
