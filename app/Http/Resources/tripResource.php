<?php

namespace App\Http\Resources;

use App\Models\TripComment;
use App\Models\TripRate;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

class tripResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::where( 'api_token' , request()->headers->get('apiToken') )->first();

        if ($user){
            $rateTrip = TripRate::whereUserId($user->id)->whereTripId($this->id)->first();
            $commentTrip = TripComment::whereUserId($user->id)->whereTripId($this->id)->first();

        }

        return [
            'id'            => $this->id,
            'price'         => $this->price,
            'name'          => $this->name,
            'date'          => $this->date,
            'phone'         => $this->phone,
            'email'         => $this->email,
            'my_rate'       => $this->when( $user && isset($rateTrip) ,  isset($rateTrip) ? $rateTrip->rate : 0 ),
            'my_comment'       => $this->when( $user && isset($commentTrip) ,  isset($commentTrip) ? $commentTrip->comment : ' ' ),
            'can_rate'      => $this->can_rate == 1 ? true: false,
            'can_comment'   => $this->can_comment == 1 ? true: false,
            'category'      => $this->when(  $this->category , new categoryRecource($this->category )) ,
            'city'          => $this->when(  $this->city , new CityRecource($this->city )),
            'images'        => $this->when( count($this->images) > 0 ,    imagesResource::collection($this->images))
        ];
    }
}
