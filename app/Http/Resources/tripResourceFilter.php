<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class tripResourceFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'price'     => $this->price,
            'name'      => $this->name,
            'image'     => $this->when( count($this->images) > 0 ,  \URL::to('/') . $this->images[0]['url'])
        ];
    }
}
