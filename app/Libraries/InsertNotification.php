<?php
namespace App\Libraries;

use App\Libraries\PushNotification;
use App\Models\Device;
use  App\Models\Notification;

class InsertNotification {

    public $push;
    function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

    public  function NotificationDbType($type  ,$user,  $sender = null ,  $request = null  , $order = null ,$additional = null){

            $admins= Device::whereDeviceType('web')->pluck('user_id');
            $devicesWeb= Device::whereDeviceType('web')->pluck('device');
             $userIos= Device::where('user_id','!=',$sender)->whereDeviceType('Ios')->pluck('user_id');

            switch($type){

            case $type == 1:

                // contactUs Form admin
                //  $sender  admin who send
                // admin panel
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'title' =>   trans('global.connect_us')  ,
                    'body' => $request ,
                    'type' => 1,
                  ];
                  $this->insertData($data);
             break;

            case $type == 2:

                     // contactUs Form Users
                  foreach ($admins as $admin){
                         $data = [
                           'user_id' => $admin ,
                           'sender_id' => $sender ,
                           'title' => trans('global.connect_us'),
                           'body' => $request ,
                           'type' => 2,
                         ];
                         $this->insertData($data);
                  }
                $this->push->sendPushNotification(null, $devicesWeb, 'تواصل معنا', $request);
                  break;

            case $type == 3:

                 $data = [
                    'user_id' => $user->id ,
                    'sender_id' => $sender->id,
                    'order_type' => 'product',
                    'order_id' => $order->id,
                    'product_id' => $request,
                    'title' =>   'طلبات المنتجات '  ,
                    'body' =>   ' لديك طلب منتج جديد من المستخدم ' .   $sender->name   ,
                    'type' => 3,
                ];
                $this->insertData($data);

             break;

            case $type == 4:

                $data = [
                    'user_id' => $user->id ,
                    'sender_id' => $sender->id,
                    'order_type' => 'product',
                    'order_id' => $order->id,
                    'product_id' => $request,
                    'title' =>   'طلبات المنتجات'  ,
                    'body' =>  ' تم قبول طلب المنتج من مزود الخدمة ' .   $sender->name  ,
                    'type' => 4,
                ];
                $this->insertData($data);
             break;

            case $type == 5:

                $data = [
                    'user_id' => $user->id ,
                    'sender_id' => $sender->id,
                    'order_type' => 'product',
                    'order_id' => $order->id,
                    'product_id' => $order->product_id,
                    'title' =>   'طلبات المنتجات'  ,
                    'body' =>  ' تم رفض طلب المنتج من مزود الخدمة ' .   $sender->name .' بسبب  ' . $request  ,
                    'type' => 5,
                ];
                $this->insertData($data);

             break;

            case $type == 6:

                $data = [
                    'user_id'       => $user->id ,
                    'sender_id'     => $sender->id,
                    'order_type'    => 'project',
                    'order_id'      => $order->id,
                    'title'         =>   ' طلبات المشاريع  '  ,
                    'body'          =>  ' يوجد طلب مشروع جديد من ' .   $sender->name ,
                    'type'          => 6,
                ];
                $this->insertData($data);

             break;
             
             case $type == 7:

                $data = [
                    'user_id'       => $user ,
                    'sender_id'     => $sender->id,
                    'order_type'    => 'project',
                    'order_id'      => $order->id,
                    'offer_id'      => $request->id,
                    'title'         =>   'العروض '  ,
                    'body'          =>     $order->id .' علي المشروع رقم  '. $sender->name .' تم قبول عرضك من المستخدم   '  ,
                    'type'          => 7,
                ];
                $this->insertData($data);

             break;

             case $type == 8:

                $data = [
                    'user_id'       => $user ,
                    'sender_id'     => 1,
                    'order_type'    => 'project',
                    'order_id'      => $order->id,
                    'title'         =>   'العروض '  ,
                    'body'          =>   $order->id . ' تم انتهاء من التوقيت يمكنك الأن تفحص العروض علي طلبك رقم  '  ,
                    'type'          => 8,
                ];
                $this->insertData($data);

             break;


             case $type == 20:
                 $data = [
                     'user_id'      => 1,
                     'sender_id'    => $sender->id,
                     'order_type'   => 'product',
                     'order_id'     => $order->id,
                     'product_id'   => $order->product_id,
                     'title'        =>   'طلبات المنتجات'  ,
                     'body'         =>  ' يوجد طلب تحويل جديد من المستخدم ' .   $sender->name  ,
                     'type'         => 20,
                 ];
                 $this->insertData($data);
             break;

             case $type == 21:
                 $data = [
                     'user_id'      => 1 ,
                     'sender_id'    => $sender->id,
                     'order_type'   => 'project',
                     'order_id'     => $order->id,
                     'title'        =>   'طلبات المشايع'  ,
                     'body'         =>  ' يوجد طلب تحويل جديد من المستخدم ' .   $sender->name  ,
                     'type'         => 21,
                 ];
                 $this->insertData($data);
             break;

            case $type == 22:
                $data = [
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'order_type'   => 'product',
                    'order_id'     => $order->id,
                    'product_id'   => $order->product_id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم قبول التحويل الخاص بي المنتج ويمكنك الأن بدا المحادثة علي الطلب رقم ' .    $order->id ,
                    'type'         => 22,
                ];
                $this->insertData($data);
            break;

            case $type == 23:
                $data = [
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'order_type'   => 'project',
                    'order_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم قبول التحويل الخاص بي المشروع ويمكنك الأن بدا المحادثة علي الطلب رقم ' .    $order->id ,
                    'type'         => 23,
                ];
                $this->insertData($data);
            break;

            case $type == 24:
                $data = [
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'order_type'   => 'product',
                    'order_id'     => $order->id,
                    'product_id'   => $order->product_id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم رفض التحويل علي طلبك من المنتج  رقم ' .    $order->id  .' بسبب  ' . $request,
                    'type'         => 24,
                ];
                $this->insertData($data);
            break;

            case $type == 25:
                $data = [
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'order_type'   => 'project',
                    'order_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم رفض التحويل علي طلبك من المشروع  رقم ' .    $order->id  .' بسبب  ' . $request,
                    'type'         => 24,
                ];
                $this->insertData($data);
            break;


            default:

     }


    }


    private function insertData($data)
    {
     if (count($data) > 0) {

         $time = ['created_at' => now(),'updated_at' => now()];
           Notification::insert($data   + $time);

     }
    }


}
       
       