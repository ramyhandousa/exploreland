<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    public $timestamps = false;

    protected $fillable = ['capital_ar'];


    public function children()
    {
        return $this->hasMany(Country::class, 'parent_id');
    }


    public function parent(){
        return $this->belongsTo(Country::class,'parent_id');
    }
}
