<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class NewAdmin extends Model
{

    use Translatable;
    protected $table = 'news';


    public $translatedAttributes = ['name','description'];

    protected $hidden = [
        'translations'
    ];


}
