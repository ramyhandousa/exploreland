<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Support extends Model
{

    protected $fillable = [
        'is_read' , 'is_deleted'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sender()
    {
        return $this->belongsTo(User::class , 'sender_id');
    }







}
