<?php

namespace App\Models;

use App\TripImage;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use Translatable;

    public $translatedAttributes = ['name','description'];

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }


    public function city(){
        return $this->belongsTo(City::class,'city_id');
    }

    public function images(){
        return $this->hasMany(TripImage::class,'trip_id');
    }

}
