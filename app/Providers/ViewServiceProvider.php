<?php
/**
 * Created by PhpStorm.
 * User: Hassan Saeed
 * Date: 11/16/2017
 * Time: 9:29 AM
 */

namespace App\Providers;

use App\Libraries\Main;
use App\Models\City;
use App\Models\Setting;
use App\Models\Support;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {

            $helper = new \App\Http\Helpers\Images();
            $main_helper = new \App\Http\Helpers\Main();
            $setting = new Setting;
            $main = new Main();

            $usersCount = User::whereDoesntHave('roles')->whereDoesntHave('abilities')->where('defined_user','other')->count();
            $resturantOrCafeCount = User::whereDoesntHave('roles')->whereDoesntHave('abilities')->where('defined_user','!=','other')->count();
            $offersCountAdmin =0;
            $mealsCountAdmin = 0;
            $categoriesCountAdmin = 0;

            $userHelpAdminCount = User::where('id', '!=', \Auth::id())->whereHas('roles', function ($q) {
                $q->where('name', '!=', 'owner');
            })->count();

            $citiesCount = 0;
            $messageNotReadCount = Support::whereUserId(\Auth::id())->whereIsRead(0)->count();
            $messageReadCount =  Support::whereIsRead(1)->count();

            $view->with(
                compact('usersCount','resturantOrCafeCount','offersCountAdmin','mealsCountAdmin',
                    'categoriesCountAdmin', 'citiesCount',
                    'userHelpAdminCount','messageNotReadCount','messageReadCount',
                'helper', 'main', 'setting','main_helper','citiesCount')
            );
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}


