<?php

namespace App;

use App\Models\City;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Hash;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyAdminResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable ,HasRolesAndAbilities;

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));

    }


    protected $fillable = [
        'defined_user',
        'name',
        'phone',
        'email',
        'password',
        'api_token',
        'image',
        'is_active',
        'is_accepted',
        'is_suspend',
        'message',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','remember_token'
    ];

    public function hasAnyRoles()
    {
        if (auth()->check()) {

            if (auth()->user()->roles->count()) {
                return true;
            }

        } else {
            redirect(route('admin.login'));
        }
    }

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }


}
