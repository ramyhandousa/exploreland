@extends('admin.layouts.master')
@section('title', 'إدارة التقارير ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">

                <form action="{{route('reports.index')}}@if(request('type'))?type=projects @endif" method="GET" class="app-search">
                    {{--<a href=""><i class="fa fa-search" style="position: absolute;  top: 10px; right: 165px;color: green;"> </i></a>--}}
                    <input type="hidden" name="type" value="projects">
                    <input type="text" name="name" placeholder="Search..." class="form-control">

                </form>

            </div>
            <h4 class="page-title">{{$pageName}}</h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box  ">
                <div class="row">
                    @foreach($reports as $report)

                            <div class="col-lg-4 col-md-6">
                                <div class="card-box widget-user">
                                    <div>
                                        <a href="{{route('provider_data_report')}}?id={{$report->id}}@if(request('type'))&type=projects @endif">
                                            <img src="{{URL('/')}}{{ '/' .$report->image}}" class="img-responsive img-circle" alt="user">

                                        </a>
                                        <div class="wid-u-info">
                                            <h4 class="m-t-0 m-b-5 font-600">{{$report->name}}</h4>
                                            <p class="text-muted m-b-5 font-13">{{$report->email}}</p>
                                            <small class="text-warning"><b>{{$report->phone}}</b></small>
                                        </div>
                                    </div>
                                </div>

                            </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>

    </script>


@endsection

