@extends('admin.layouts.master')
@section('title', 'إدارة التقارير ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">


            </div>
            <h4 class="page-title"> قائمة تقارير   {{$pageName}} </h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box rotateOutUpRight ">

                <div class="row">
                    <h3 class="page-title">      مزود الخدمة هو   {{isset( $provider ) ? $provider->name : ''}}   </h3>
                    <h4>  الرقم الذي يجب  دفعه هو {{$payForProvider}}</h4>
                </div>
                <br>

                <table id="datatable-fixed-header" class="table  table-striped">
                    <thead>
                    <tr>
                        <th>رقم الطلب</th>
                        <th>اسم المستخدم  </th>
                        <th>تاريخ   الطلب </th>
                        <th>      تفاصيل    </th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($reports as $report)

                        <tr>

                            <td> {{$report->id}}  </td>
                            <td>  {{  optional( $report->user)->name}} </td>
                            <td>  {{ $report->created_at ? $report->created_at->format('Y-m-d') :" لا يوجد تاريخ  " }} </td>
                            <td>
                                <button class="btn btn-primary waves-effect waves-light"
                                        data-toggle="modal"
                                        data-target="#tabs-modal{{$report->id}}">مشاهدة</button>

                            </td>

                            <div id="tabs-modal{{$report->id}}" class="modal fade" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content p-0">
                                        <ul class="nav nav-tabs navtab-bg nav-justified">
                                            <li class="active">
                                                <a href="#home-2{{$report->id}}" data-toggle="tab" aria-expanded="false">
                                                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                                                    <span class="hidden-xs">المعلومات الأساسية</span>
                                                </a>
                                            </li>
                                            @if(request('type') == 'projects')
                                                <li class="">
                                                    <a href="#profile-2{{$report->id}}" data-toggle="tab" aria-expanded="false">
                                                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                                                        <span class="hidden-xs">بيانات المشروع</span>
                                                    </a>
                                                </li>

                                             @else


                                                <li class="">
                                                    <a href="#settings-2{{$report->id}}" data-toggle="tab" aria-expanded="false">
                                                        <span class="visible-xs"><i class="fa fa-cog"></i></span>
                                                        <span class="hidden-xs">بيانات طلب الخاص بي المنتج</span>
                                                    </a>
                                                </li>
                                            @endif


                                            <li class="">
                                                <a href="#messages-2{{$report->id}}" data-toggle="tab" aria-expanded="true">
                                                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                                                    <span class="hidden-xs">الحسابات</span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="home-2{{$report->id}}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-1" class="control-label">إسم المستخدم صاحب الطلب</label>
                                                            <input type="text"  value="{{  optional( $report->user)->name}}" class="form-control"  readonly>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">تاريخ الطلب   </label>
                                                            <input type="text"  value="{{   $report->created_at ? $report->created_at->format('Y-m-d') :" لا يوجد تاريخ  " }}"  class="form-control" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">حالة الطلب </label>
                                                            <input type="text"  value="{{   $report->status_translation }}"  class="form-control" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="profile-2{{$report->id}}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-1" class="control-label">عنوان المشروع</label>
                                                            <input type="text"  value="{{    $report->title }}" class="form-control"  readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">تفاصيل المشروع </label>
                                                            <textarea class="form-control"  readonly>{{$report->description}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">تاريخ استلام المشروع  </label>
                                                            <input type="text" value=" {{  $report->received_date  }}"  class="form-control"  readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">تم مراجعته من قبلك  </label>
                                                            <input type="text" value=" {{  $report->is_review == 0 ? 'لم يتم مراجعته حتي الأن'  : ' تم مراجعته' }}"  class="form-control"  readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane " id="messages-2{{$report->id}}">
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">تكلفة الطلب </label>
                                                            <input type="text"  value="{{   $report->price }}"  class="form-control"   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">النسبة الخاصة بالتطبيق </label>
                                                            <input type="text" value=" {{    $report->app_percentage  }}" class="form-control"readonly>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">صافي ربحك   في الطلب  </label>
                                                            <input type="text"  value="{{  $report->adminPrice   }}"  class="form-control" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">صافي ربح مزود الخدمة في الطلب  </label>
                                                            <input type="text"  value="{{  $report->providerPrice   }}"  class="form-control" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">حالة الدفع (    إيصال  – دفع إلكتروني)  </label>
                                                            <input type="text"  value="{{    $report->payment == 'cash' ? 'إيصال بنكي'  : 'دفع إلكتروني' }}"  class="form-control" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="settings-2{{$report->id}}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">إسم المنتج   </label>
                                                            <input type="text"  value="{{   optional($report->product)->name }}"  class="form-control"   readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">تم مراجعته من قبلك  </label>
                                                            <input type="text" value=" {{  $report->is_review == 0 ? 'لم يتم مراجعته حتي الأن'  : ' تم مراجعته' }}"  class="form-control"  readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="field-2" class="control-label">تم دفع الطلب  </label>
                                                            <input type="text" value=" {{  $report->is_pay == 0 ? 'لم يتم دفعه حتي الأن'  : ' تم دفعه' }}"  class="form-control"  readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->


                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>


    </script>


@endsection

