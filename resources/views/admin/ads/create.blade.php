@extends('admin.layouts.master')
@section('title', __('maincp.users_manager'))


@section('styles')


    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">


@endsection
@section('content')



    <form method="POST" action="{{ route('ads.store') }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة {{$pageName}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">

                    <h4 class="header-title m-t-0 m-b-30"> إضافة {{$pageName}}</h4>

                    <div class="row">

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">   عنوان  للإعلان    </label>
                                <input type="text" name="title"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="    عنوان  للإعلان  ..."/>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('name_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('name_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">   لينك  للإعلان    </label>
                                <input type="url" name="link"
                                       class="form-control requiredFieldWithMaxLenght" data-parsley-type="url"
                                       data-parsley-type-message="يجب ادخال   لينك للإعلان صحيح من فضلك "
                                       required
                                       placeholder="    لينك  للإعلان ."/>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('name_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('name_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">

                            <div class="form-group">
                                <label for="usernames">صورة الإعلان  </label>
                                <input type="file" name="image" class="dropify" data-max-file-size="6M"/>
                            </div>

                        </div>


                    </div>


                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    <script src="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>


    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">

        function validImages() {

             var images = $( "input[name='image']" ).val();
            if (  images === undefined || images === ""  ) {
                errorMessageTostar(' من فضلك ','من فضلك إختار   صورة   ');
                return false;
            }
            return true;
        }

        $(document).ready(function() {

            $('.js-example-basic-multiple').select2();


        });

        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid() && validImages()){

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    beforeSend:function () {
                        $('.loading').show();

                    },
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                         messageDisplay( 'نجاح' , data.message)
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 2000);
                    },
                    error: function (data) {
                        $('.loading').hide();
                    }
                });
            }else {
                $('.loading').hide();
            }
        });

       function messageDisplay($title, $message) {
           var shortCutFunction = 'success';
           var msg = $message;
           var title = $title;
           toastr.options = {
               positionClass: 'toast-top-left',
               onclick: null
           };
           var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
           $toastlast = $toast;
       }

       function errorMessageTostar($title, $message) {
           var shortCutFunction = 'error';
           var msg = $message;
           var title = $title;
           toastr.options = {
               positionClass: 'toast-top-left',
               onclick: null
           };
           var $toast = toastr[shortCutFunction](msg, title);
           $toastlast = $toast;
       }

    </script>
@endsection

