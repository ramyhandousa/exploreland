@extends('admin.layouts.master')
@section('title', __('maincp.users_manager'))
@section('styles')

    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">

@endsection

@section('content')


    <form method="POST" action="{{ route('ads.update', $ads->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}


    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12 ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">  {{$pageName}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">

                    <h4 class="header-title m-t-0 m-b-30">   {{$pageName}}</h4>

                    <div class="row">

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">   عنوان  للإعلان    </label>
                                <input type="text" name="title" value="{{$ads->title}}"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="   باللغة العربية{{$pageName}}  ..."/>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('name_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('name_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">   لينك  للإعلان    </label>
                                <input type="url" name="link" value="{{$ads->link}}"
                                       class="form-control requiredFieldWithMaxLenght" data-parsley-type="url"
                                       data-parsley-type-message="يجب ادخال   لينك للإعلان صحيح من فضلك "
                                       required
                                       placeholder="   باللغة العربية{{$pageName}}  ..."/>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('name_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('name_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-xs-6">

                            <div class="form-group">
                                <label for="usernames">صورة الإعلان  </label>
                                <input type="hidden" value="{{ $ads->url }}" name="oldImage"/>

                                <input type="file" name="image" class="dropify" data-max-file-size="6M"
                                       data-default-file="{{ request()->root().$ads->url }}"/>
                            </div>

                        </div>


                    </div>


                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    <script src="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>


    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">

        function validImages() {

            var oldImages = $( "input[name='oldImage']" ).val();
            var images = $( "input[name='image']" ).val();


            if ( (images === undefined || images === "" ) &&  (oldImages === undefined || oldImages === "" )  ) {
                errorMessageTostar(' من فضلك ','من فضلك إختار   صورة واحدة علي الأقل  ');
                return false;
            }
            return true;
        }

        $(document).ready(function() {

            $(document).on('change', '.category', function () {
                var City_id =   $(this).val();
                var div =   $(this).parent().parent().parent();
                var op =" ";
                var showElement =  div.find('.showElement');
                $.ajax({
                    type:"Get",
                    url: "{{route('getSub')}}",
                    data: {'id': City_id},
                    success:function (data) {

                        if (data.length === undefined){
                            errorMessageTostar('لا يوجد','من فضلك إختار   دولة اخري ');
                        }else {
                            op += '<option  disabled selected>إختر مدينة   </option>';
                            for (var i= 0 ; i <data.length ; i ++){
                                op += '<option value="'+ data[i].id +'">' + data[i].name + '</option>';
                            }
                            div.find('.City_chid').html(" ");
                            div.find('.City_chid').append(op);
                            if (data == null || data == undefined || data.length == 0){
                                showElement.delay(500).slideUp();
                            }else {
                                showElement.delay(500).slideDown();

                            }
                        }

                    },
                    error:function (error) {
                        console.log(error)
                    }
                })
            });

        });


        $('body').on('click', '.removeElement', function () {


            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            swal({
                title: "هل انت متأكد؟",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status == true) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت عملية الحذف بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

//                                $("#image-works" + id)[0].children[2].children[4].click();
                                window.location.reload();

//                                $("#elementRow" + id).remove();
                            }
                            if (data.status == false) {
                                var shortCutFunction = 'warning';
                                var msg =data.message;
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                            }
                        }
                    });
                }
            });
        });

        $(document).ready(function() {

            $('.js-example-basic-multiple').select2();

            $(function() {
                $("#select_date").on('change', function(){
                    var date = Date.parse($(this).val());

                    if (date < Date.now()   ){
                        errorMessageTostar(' خطأ في التاريخ ','من فضلك إختار تاريخ قدام ');
                        $(this).val('');
                    }
                });
            });

        });

        $('input[name=is_rate]').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.priceOffer').show();
            } else {
                $('.priceOffer').hide();
            }
        });

        $('input[name=is_comment]').on('click', function (e) {

            if ($(this).is(':checked')) {
                $('.dealField').show();
            } else {
                $('.dealField').hide();
            }
        });





        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid() && validImages()){

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    beforeSend:function () {
                        $('.loading').show();

                    },
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                        messageDisplay( 'نجاح' , data.message)
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 2000);
                    },
                    error: function (data) {
                        $('.loading').hide();
                    }
                });
            }else {
                $('.loading').hide();
            }
        });

        function messageDisplay($title,$message) {
            var shortCutFunction = 'success';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
            $toastlast = $toast;
        }

        function errorMessageTostar($title, $message) {
            var shortCutFunction = 'error';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            var $toast = toastr[shortCutFunction](msg, title);
            $toastlast = $toast;
        }

    </script>
@endsection