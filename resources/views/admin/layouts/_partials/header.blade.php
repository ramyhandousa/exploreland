<!-- Navigation Bar-->
<header id="topnav">

    <div class="topbar-main">
        <div class="container">

            <!-- LOGO -->
            <div class="topbar-left">
                {{--<a href="{{ route('admin.home') }}" class="logo" style="width: 150px;">--}}
                    {{--<img style="width: 100%" src="{{ request()->root() }}/public/assets/admin/images/logo.png"></a>--}}
            </div>
            <!-- End Logo container-->


            <div class="menu-extras">

                <ul class="nav navbar-nav navbar-right pull-right">


                    @if(\Illuminate\Support\Facades\Auth::check())
                    <li class="dropdown user-box">
                        <a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown"
                           aria-expanded="true">
                            <img src="{{ $helper->getDefaultImage(auth()->user()->image, request()->root().'/public/assets/admin/images/default.png') }}"
                                 alt="user-img" class="img-circle user-img">
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="{{ route('helpAdmin.edit', auth()->user()->id)}}"><i
                                            class="ti-user m-r-5"></i>@lang('maincp.personal_page')</a></li>
{{--                            <!--<li><a href="{{ route('users.edit', auth()->id()) }}"><i class="ti-settings m-r-5"></i>-->--}}
                            <!--        @lang('global.settings')-->
                            <!--    </a>-->
                            <!--</li>-->
                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="ti-power-off m-r-5"></i>@lang('maincp.log_out')
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

        </div>
    </div>

    <form id="logout-form" action="{{ route('administrator.logout') }}" method="POST"
          style="display: none;">
        {{ csrf_field() }}
    </form>

    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu" style="    font-size: 14px;">
                    {{--@can('statistics_manage')--}}
                    
                    {{--<li>--}}
                        {{--<a href="{{ route('admin.home') }}"><i class="zmdi zmdi-view-dashboard"></i>--}}
                            {{--<span> @lang('menu.home') </span> </a>--}}
                    {{--</li>--}}
                     {{--@endcan--}}



                    @can('users_manage')
                        <li class="has-submenu">
                            <a href="{{ route('users.index') }}"><i
                                        class="zmdi zmdi-accounts"></i><span>إدارة المستخدمين   </span>
                            </a>
                            {{--<ul class="submenu ">--}}
                                {{--<li>  <a href="{{ route('users.index') }}?type=clients">  المستخدمين </a>   </li>--}}
                                {{--<li>  <a href="{{ route('users.index') }}?type=provider">  مزودي الخدمات   </a>   </li>--}}
                            {{--</ul>--}}
                        </li>

                    @endcan


                     @can('content_manage')
                        <li class="has-submenu">
                            <a href="javascript:;">
                                <i class="zmdi zmdi-square-o"></i>
                                <span>  إعدادات النظام </span>
                            </a>
                            <ul class="submenu ">
                                <li><a href="{{ route('cities.index') }}?type=countery">الدولة</a></li>
                                <li><a href="{{ route('cities.index') }}">المدينة</a></li>
                              </ul>
                        </li>
                    @endcan

                    <li class="has-submenu">
                        <a href="{{ route('ads.index') }}">
                            <i class="zmdi zmdi-accounts"></i><span>إدارة الإعلانات التروجية   </span>
                        </a>
                    </li>

                    <li class="has-submenu">
                        <a href="{{ route('categories.index') }}">
                            <i class="zmdi zmdi-accounts"></i><span>إدارة الأقسام   </span>
                        </a>
                    </li>

                    <li class="has-submenu">
                        <a href="{{ route('places.index') }}">
                            <i class="zmdi zmdi-accounts"></i><span>إدارة الأمكان   </span>
                        </a>
                    </li>

                    <li class="has-submenu">
                        <a href="{{ route('newsAdmin.index') }}"><i
                                    class="zmdi zmdi-accounts"></i><span>إدارة الأخبار   </span>
                        </a>

                    </li>



                    @can('settings_manage')


                        <li class="has-submenu">
                            <a href="#"><i class="zmdi zmdi-settings"></i><span>@lang('maincp.setting')<i
                                            class="fa fa-arrow-down visible-xs" aria-hidden="true"></i></span> </a>
                            <ul class="submenu">
{{--                                <li><a href="{{ route('settings.contactus') }}">@lang('trans.general_setting_app')</a>    </li>--}}
                                <li><a href="{{ route('settings.aboutus') }}">@lang('trans.about_app')</a>   </li>
                                <li><a href="{{ route('settings.terms') }}">@lang('trans.terms') للمستخدمين</a></li>
                                <li><a href="{{ route('supports.index') }}">@lang('maincp.contact_us')</a></li>
                            </ul>
                        </li>
                    @endcan

                    {{--@can('reports_manage')--}}
                        {{--<li class="has-submenu">--}}
                            {{--<a href="#"><i class="zmdi zmdi-flag"></i><span>@lang('maincp.reports')</span> </a>--}}
                            {{--<ul class="submenu">--}}
                                {{--<li>--}}
                                    {{--<a href="{{ route('reports.index') }}">         تقارير طلبات المنتجات   </a>--}}
                                    {{--<a href="{{ route('reports.index') }}?type=projects">         تقارير طلبات المشاريع   </a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    {{--@endcan--}}

                </ul>
                <!-- End navigation menu  -->
            </div>
        </div>
    </div>

</header>
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container">
