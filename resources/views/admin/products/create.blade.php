@extends('admin.layouts.master')
@section('title', __('maincp.users_manager'))


@section('styles')


    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
    <link href="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">


@endsection
@section('content')



    <form method="POST" action="{{ route('places.store') }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}

    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>
                </div>
                <h4 class="page-title">إدارة {{$pageName}}</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">


                    <h4 class="header-title m-t-0 m-b-30"> إضافة {{$pageName}}</h4>

                    <div class="row">




                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">إسم القسم الرئيسي*</label>
                                <select name="categoryId"  class="form-control requiredFieldWithMaxLenght"
                                        required
                                >
                                    <option value="" selected disabled=""> إختر القسم</option>

                                    @foreach($categories as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group ">
                                <label  >  الدولة :</label>
                                <select class="form-control  category   "   name="cityId" >
                                    <option value="" selected disabled=""> إختر الدولة</option>
                                    @foreach($cities as  $value)
                                        <option  value="{{ $value->id }}" >   {{ $value->name }}  </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group m-form__group row" >
                                <label  >  المدينة:</label>
                                <select class="form-control City_chid requiredFieldWithMaxLenght"  name="cityId" required>
                                </select>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="userPhone">رقم للتواصل واتساب   *</label>
                                <input type="number" name="phone" value="{{ old('phone') }}" class="form-control"
                                       required
                                       data-parsley-maxLength="11"
                                       data-parsley-maxLength-message=" رقم للتواصل  يجب أن يكون 11   فقط"
                                       data-parsley-minLength="11"
                                       data-parsley-minLength-message=" رقم للتواصل  يجب أن يكون   11 حروف "
                                       data-parsley-required-message="يجب ادخال   رقم للتواصل"
                                       placeholder="  رقم للتواصل..."
                                />
                                @if($errors->has('phone'))
                                    <p class="help-block">
                                        {{ $errors->first('phone') }}
                                    </p>
                                @endif
                            </div>
                        </div>


                        <div class="col-xs-4">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="emailAddress">البريد الإلكتروني للتواصل*</label>

                                <input type="email" name="email" parsley-trigger="change" value="{{ old('email') }}"
                                       class="form-control"
                                       placeholder="البريد الإلكتروني..."
                                       data-parsley-type="email"
                                       data-parsley-type-message="أدخل البريد الالكتروني بطريقة صحيحة"
                                       data-parsley-required-message="يجب ادخال  البريد الالكتروني"
                                       data-parsley-maxLength="30"
                                       data-parsley-maxLength-message=" البريد الالكتروني  يجب أن يكون ثلاثون حرف فقط"
                                       {{--data-parsley-pattern="/^([a-z0-9_\.-]+\@[\da-z\.-]+\.[a-z\.]{2,6})$/gm"--}}
                                       {{--data-parsley-pattern-message="أدخل  البريد الالكتروني بطريقة الايميل ومن غير مسافات"--}}
                                       required
                                />
                                @if($errors->has('email'))
                                    <p class="help-block">{{ $errors->first('email') }}</p>
                                @endif
                            </div>
                        </div>



                        <div class="col-xs-4">
                            <div class="form-group">
                                <label for="userName">سعر مكان *</label>
                                <input type="number" name="price"
                                       required
                                       min=1 oninput="validity.valid||(value='');" class="form-control requiredFieldWithMaxLenght" >

                            </div>
                        </div>

                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="userName">   إسم المكان    </label>
                                <input type="text" name="name_ar"
                                       class="form-control requiredFieldWithMaxLenght"
                                       required
                                       placeholder="   باللغة العربية{{$pageName}}  ..."/>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('name_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('name_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">

                                <label>تاريخ   الرحلة</label>
                                <input type="text" name="date_trip" id="select_date"
                                       required data-parsley-required-message="يجب ادخال  تاريخ الرحلة من فضلك"
                                       class="form-control datepicker  " />

                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName"> وصف    المكان</label>
                                <textarea type="text" name="description_ar" class="form-control m-input requiredFieldWithMaxLenght"
                                          required
                                          placeholder="إدخل  وصف عن  المكان     "   ></textarea>
                                <p class="help-block" id="error_userName"></p>
                                @if($errors->has('description_ar'))
                                    <p class="help-block">
                                        {{ $errors->first('description_ar') }}
                                    </p>
                                @endif
                            </div>
                        </div>



                        <div class="col-xs-12">

                            @for ($i = 0; $i < 4; $i++)
                                <div class="col-xs-3">
                                <div class="form-group">
                                    <label for="usernames">صورة المكان  </label>
                                    <input type="file" name="image[]" class="dropify" data-max-file-size="6M"/>
                                </div>
                                </div>
                            @endfor

                        </div>

                        <div class="col-xs-12">
                            <div class="form-group col-lg-6 ">
                                <label>هل يوجد تقيم؟</label>
                                <input type="checkbox" name="is_rate" value="1"  />
                            </div>
                            <div class="form-group col-lg-6 priceOffer" style="display: none">
                                <label>   سوف يستطيع المستخدم تقيم هذا المكان </label>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <div class="form-group col-lg-6">
                                <label>هل يوجد تعليق ؟</label>
                                <input type="checkbox" name="is_comment" value="1"  class="styled" />
                            </div>
                            <div class="form-group col-lg-6 dealField" style="display: none">

                                <label>   سوف يستطيع المستخدم التعليق علي هذا المكان </label>

                            </div>
                        </div>

                    </div>


                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-warning waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    <script src="{{ request()->root() }}/public/assets/admin/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>


    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">

        function validImages() {

             var images = $( "input[name='image[]']" ).val();
            if (  images === undefined || images == ""  ) {
                errorMessageTostar(' من فضلك ','من فضلك إختار   صورة واحدة علي الأقل  ');
                return false;
            }
            return true;
        }

        $(document).ready(function() {

            $(document).on('change', '.category', function () {
                var City_id =   $(this).val();
                var div =   $(this).parent().parent().parent();
                var op =" ";
                var showElement =  div.find('.showElement');
                $.ajax({
                    type:"Get",
                    url: "{{route('getSub')}}",
                    data: {'id': City_id},
                    success:function (data) {

                        if (data.length === undefined){
                            errorMessageTostar('لا يوجد','من فضلك إختار   دولة اخري ');
                        }else {
                            op += '<option  disabled selected>إختر مدينة   </option>';
                            for (var i= 0 ; i <data.length ; i ++){
                                op += '<option value="'+ data[i].id +'">' + data[i].name + '</option>';
                            }
                            div.find('.City_chid').html(" ");
                            div.find('.City_chid').append(op);
                            if (data == null || data == undefined || data.length == 0){
                                showElement.delay(500).slideUp();
                            }else {
                                showElement.delay(500).slideDown();

                            }
                        }

                    },
                    error:function (error) {
                        console.log(error)
                    }
                })
            });

        });




//        $(document).ready(function() {

            $('.js-example-basic-multiple').select2();

            $(function() {
                $("#select_date").on('change', function(){

                    var date = Date.parse($(this).val());
                    var dateObj = new Date();
                    if (date  < dateObj.setDate(dateObj.getDate() - 1)   ){
                        errorMessageTostar(' خطأ في التاريخ ','من فضلك إختار تاريخ قدام ');
                        $(this).val('');
                    }
                });
            });

//        });

        $('input[name=is_rate]').on('click', function (e) {
            if ($(this).is(':checked')) {
                $('.priceOffer').show();
            } else {
                $('.priceOffer').hide();
            }
        });

        $('input[name=is_comment]').on('click', function (e) {

            if ($(this).is(':checked')) {
                $('.dealField').show();
            } else {
                $('.dealField').hide();
            }
        });





        $('form').on('submit', function (e) {

            e.preventDefault();

            var formData = new FormData(this);

            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid() && validImages()){

                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    beforeSend:function () {
                        $('.loading').show();

                    },
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('.loading').hide();
                         messageDisplay( 'نجاح' , data.message);
                        setTimeout(function () {
                            window.location.href = data.url;
                        }, 2000);
                    },
                    error: function (data) {
                        $('.loading').hide();
                    }
                });
            }else {
                $('.loading').hide();
            }
        });

       function messageDisplay($title, $message) {
           var shortCutFunction = 'success';
           var msg = $message;
           var title = $title;
           toastr.options = {
               positionClass: 'toast-top-left',
               onclick: null
           };
           var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
           $toastlast = $toast;
       }

       function errorMessageTostar($title, $message) {
           var shortCutFunction = 'error';
           var msg = $message;
           var title = $title;
           toastr.options = {
               positionClass: 'toast-top-left',
               onclick: null
           };
           var $toast = toastr[shortCutFunction](msg, title);
           $toastlast = $toast;
       }

    </script>
@endsection

