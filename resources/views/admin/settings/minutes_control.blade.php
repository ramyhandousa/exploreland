@extends('admin.layouts.master')
@section('title' ,__('maincp.about_us'))
@section('content')
    <form action="{{ route('administrator.settings.store') }}" data-parsley-validate="" novalidate="" method="post"
          enctype="multipart/form-data">

    {{ csrf_field() }}

    <!-- Page-Title -->

        <div class="row">
            <div class="col-sm-12 ">
                <div class="btn-group pull-right m-t-15">
                    <button type="button" class="btn btn-custom  waves-effect waves-light"
                            onclick="window.history.back();return false;"> @lang('maincp.back')<span class="m-l-5"><i
                                    class="fa fa-reply"></i></span>
                    </button>

                </div>
                <h4 class="page-title">التوقت  بالدقايق لظهور العروض للمستخدم  </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <div class="col-xs-6">
                        <div class="form-group ">
                            <img style="height: 200px" src="{{url('/').'/public/ddg-timer.gif'}}">
                        </div>
                    </div>

                    <div class="col-xs-6" style="margin-bottom: 150px">
                        <div class="form-group {{ $errors->has('minutes_control') ? 'has-error' : '' }}">
                            <label for="about_us_en">الدقايق   </label>
                            <input type="number" min="2" name="minutes_control"    max="180" required
                                   data-parsley-required-message="  التوقيت مطلوب"
                                   data-parsley-min-message="أقل توقيت   يجب ان يكون 2 دقيقة"
                                   data-parsley-maxlength-message=" أقصى عدد الحروف المسموح بها هى (180) حرف"
                                   value="{{ $setting->getBody('minutes_control') }}"
                                   class="form-control " >
                        </div>
                    </div>

                    <div class="form-group text-right m-t-20">
                        <button class="btn btn-primary waves-effect waves-light m-t-20" type="submit">
                            @lang('maincp.save_data')
                        </button>
                        <button onclick="window.history.back();return false;" type="reset"
                                class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                            @lang('maincp.disable')
                        </button>
                    </div>

                </div>
            </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>
@endsection


@section('scripts')
    <script type="text/javascript"
            src="{{ request()->root() }}/public/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>
    <script type="text/javascript">

        $('form').on('submit', function (e) {
            e.preventDefault();
            var formData = new FormData(this);
            var form = $(this);
            form.parsley().validate();

            if (form.parsley().isValid()) {
                $.ajax({
                    type: 'POST',
                    url: $(this).attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        //  $('#messageError').html(data.message);

                        var shortCutFunction = 'success';
                        var msg = ' تم إحتساب الوقت المحدد بنجاح';
                        var title = 'نجاح';
                        toastr.options = {
                            positionClass: 'toast-top-left',
                            onclick: null
                        };
                        var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                        $toastlast = $toast;
                        {{--setTimeout(function () {--}}
                        {{--window.location.href = '{{ route('categories.index') }}';--}}
                        {{--}, 3000);--}}
                    },
                    error: function (data) {

                    }
                });
            }
        });

    </script>
@endsection




