<?php

use Illuminate\Http\Request;
use Stichoza\GoogleTranslate\GoogleTranslate;


Route::group([
    'namespace' => 'Api',
], function () {


    Route::group(['prefix' => 'Auth'], function () {

        Route::post('login','LoginController@login');
        Route::post('register', 'RegisterController@register');
        Route::post('forgetPassword', 'ForgotPasswordController@forgetPassword');
        Route::post('resetPassword', 'ForgotPasswordController@resetPassword');
        Route::post('checkCode', 'ResetPasswordController@checkCodeActivation');
        Route::post('resendCode', 'ResetPasswordController@resendCode');
        Route::post('changPassword', 'ResetPasswordController@changPassword')->middleware('apiToken');
        Route::post('editProfile', 'UserController@editProfile')->middleware('apiToken');
        Route::post('logOut','LoginController@logOut')->middleware('apiToken');

    });

    Route::group(['prefix' => 'Home'], function () {

        Route::get('/','HomeController@index')->middleware('apiToken');

    });

    Route::resource('/my_trips','TripController');
    Route::get('/trip_category','TripController@trip_category');
    Route::post('/rate_trip','TripController@rate_trip')->middleware('apiToken');
    Route::post('/comment_trip','TripController@comment_trip')->middleware('apiToken');


    Route::group(['prefix' => 'setting'], function () {
        Route::get('about_us','SettingController@aboutUs');
        Route::get('terms_user','SettingController@terms_user');
        Route::post('contact_us','SettingController@contactUs');
    });


    Route::get('country','HomeController@country');

});
